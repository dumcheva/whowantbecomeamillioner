import {Questions} from "../_fakeApi/Questions";

class QuestionsService {

    getRandomOne() {
        return Questions[Math.floor(Math.random() * Questions.length)];
    }
}

export default new QuestionsService;
